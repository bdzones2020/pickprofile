<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('/');

Route::view('messages', 'posts.messages')->name('messages');
//Route::livewire('messages', 'posts.messages')->name('messages');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// user routes
Route::group(['prefix' => 'user', 'as' => 'user.', 'middleware' => ['user']], function () {
	Route::get('/profile', 'Profile\ProfileController@profile')->name('profile');

	// ajax
	Route::get('/get-states', 'User\ProfileController@getStates')->name('get-states');
});

// recruiter routes
Route::group(['prefix' => 'recruit', 'as' => 'recruit.', 'middleware' => ['recruit']], function () {
	//
});

// admin routes
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['admin']], function () {
	// dashboard
	Route::get('/dboard', 'Dboard\DboardController@dboard')->name('dboard');
});

// system routes
Route::group(['prefix' => 'system', 'as' => 'system.', 'middleware' => ['system']], function () {
	//
});