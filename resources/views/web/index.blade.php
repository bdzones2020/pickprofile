@extends('layouts.app')

@section('styles')

	<link href="{{ asset('css/profile.css') }}" rel="stylesheet">

@endsection

@section('content')

@php $appInfo = appInfo() @endphp

    <nav id="topNavbar" class="navbar navbar-expand-md bg-dark navbar-dark shadow-sm fixed-top">
        <div class="container-fluid">
            <a class="navbar-brand navbar-home" href="{{ url('/') }}">
                {{ $appInfo && $appInfo->name ? $appInfo->name : config('app.name') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('messages') }}">Messages</a>
                    </li>
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->user_name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu bg-dark dropdown-menu-right" aria-labelledby="navbarDropdown">
                                
                                @if(Auth::user()->weight >= 79.99)
                                <a href="{{ route('admin.dboard') }}" class="dropdown-item text-light bg-danger"><i class="fa fa-dashboard"></i> Dashboard</a>
                                @endif

                                <a href="{{ route('user.profile') }}" class="dropdown-item text-light"><i class="fa fa-user"></i> Profile</a>
                                <a class="dropdown-item text-light" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="main-content">
        
        @yield('web_content')
        
    </main>

@endsection

@section('scripts')

<script>
	$(document).ready(function() {
		$("#sidebarTogglerBtn").click(function(e) {
	        e.preventDefault();
	        $("#wrapper").toggleClass("toggled");
	    });
	});
</script>

@endsection