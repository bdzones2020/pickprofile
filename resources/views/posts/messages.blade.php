@extends('web.index')

@section('title', 'Messages')

@section('web_content')

@livewire('post.message')

@endsection

@section('scripts')

<script>
	window.livewire.on('imageChosen', () => {
	    var choosenImage = document.getElementById('image');
	    var imageFile = choosenImage.files[0];
	    var reader = new FileReader();

	    reader.onloadend = () => {
	    	window.livewire.emit('imageUpload',reader.result);
	    }

	    reader.readAsDataURL(imageFile);
	})
</script>

@endsection