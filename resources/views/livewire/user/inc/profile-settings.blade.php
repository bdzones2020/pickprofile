<div class="modal-content">

	<!-- Modal Header -->
	<div class="modal-header">
	  	<h4 class="modal-title">Profile Settings</h4>
	  	<button wire:click="update()" type="button" class="btn btn-success" data-dismiss="modal">Save & Close</button>
	</div>

	<!-- Modal body -->
	<div class="modal-body">
		
		<ul class="list-group">
			
			<li class="list-group-item">
				<strong class="property-name">Property</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<strong>Status</strong>
						</div>

						<div class="col-sm-3">
							<strong>Serial No</strong>
						</div>

						<div class="col-sm-6">
							<strong>Privacy</strong>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Date Of Birth</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input wire:model="dobStatus" wire:change="getPropertyIds({{ $dobId }})" type="checkbox" class="custom-control-input" id="dob_status" name="dob_status" value="1">
									<label class="custom-control-label" for="dob_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="dob_privacy" id="dob_privacy_public" value="0" wire:model="dobPrivacy" wire:change="getPropertyIds({{ $dobId }})">
									<label class="custom-control-label" for="dob_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="dob_privacy" id="dob_privacy_private" value="1" wire:model="dobPrivacy" wire:change="getPropertyIds({{ $dobId }})">
									<label class="custom-control-label" for="dob_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Interest</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" wire:model="interestStatus" wire:change="getPropertyIds({{ $interestId }})" id="interest_status" name="interest_status">
									<label class="custom-control-label" for="interest_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="interest_privacy" id="interest_privacy_public" value="0" wire:model="interestPrivacy" wire:change="getPropertyIds({{ $interestId }})">
									<label class="custom-control-label" for="interest_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="interest_privacy" id="interest_privacy_private" value="1"  wire:model="interestPrivacy" wire:change="getPropertyIds({{ $interestId }})">
									<label class="custom-control-label" for="interest_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Contact Info</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="contactInfo_status" name="contactInfo_status" wire:model="contactInfoStatus" wire:change="getPropertyIds({{ $contactInfoId }})">
									<label class="custom-control-label" for="contactInfo_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="contactInfo_privacy" id="contactInfo_privacy_public" value="0" wire:model="contactInfoPrivacy" wire:change="getPropertyIds({{ $contactInfoId }})">
									<label class="custom-control-label" for="contactInfo_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="contactInfo_privacy" id="contactInfo_privacy_private" value="1" wire:model="contactInfoPrivacy" wire:change="getPropertyIds({{ $contactInfoId }})">
									<label class="custom-control-label" for="contactInfo_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Social Network</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="socianNetwork_status" name="socianNetwork_status" wire:model="socialNetworkStatus" wire:change="getPropertyIds({{ $socialNetworkId }})">
									<label class="custom-control-label" for="socianNetwork_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">

						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="socianNetwork_privacy" id="socianNetwork_privacy_public" value="0" wire:model="socialNetworkPrivacy" wire:change="getPropertyIds({{ $socialNetworkId }})">
									<label class="custom-control-label" for="socianNetwork_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="socianNetwork_privacy" id="socianNetwork_privacy_private" value="1" wire:model="socialNetworkPrivacy" wire:change="getPropertyIds({{ $socialNetworkId }})">
									<label class="custom-control-label" for="socianNetwork_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Education</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="education_status" name="education_status" wire:model="educationStatus" wire:change="getPropertyIds({{ $educationId }})">
									<label class="custom-control-label" for="education_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="education_serial_no" name="education_serial_no" wire:model="educationSerialNo" wire:change="getPropertyIds({{ $educationId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="education_privacy" id="education_privacy_public" value="0" wire:model="educationPrivacy" wire:change="getPropertyIds({{ $educationId }})">
									<label class="custom-control-label" for="education_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="education_privacy" id="education_privacy_private" value="1" wire:model="educationPrivacy" wire:change="getPropertyIds({{ $educationId }})">
									<label class="custom-control-label" for="education_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Experience</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="experience_status" name="experience_status" wire:model="experienceStatus" wire:change="getPropertyIds({{ $experienceId }})">
									<label class="custom-control-label" for="experience_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="experience_serial_no" name="experience_serial_no" wire:model="experienceSerialNo" wire:change="getPropertyIds({{ $experienceId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="experience_privacy" id="experience_privacy_public" value="0" wire:model="experiencePrivacy" wire:change="getPropertyIds({{ $experienceId }})">
									<label class="custom-control-label" for="experience_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="experience_privacy" id="experience_privacy_private" value="1" wire:model="experiencePrivacy" wire:change="getPropertyIds({{ $experienceId }})">
									<label class="custom-control-label" for="experience_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Language</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="language_status" name="language_status" wire:model="languageStatus" wire:change="getPropertyIds({{ $languageId }})">
									<label class="custom-control-label" for="language_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="language_serial_no" name="language_serial_no" wire:model="languageSerialNo" wire:change="getPropertyIds({{ $languageId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="language_privacy" id="language_privacy_public" value="0" wire:model="languagePrivacy" wire:change="getPropertyIds({{ $languageId }})">
									<label class="custom-control-label" for="language_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="language_privacy" id="language_privacy_private" value="1" wire:model="languagePrivacy" wire:change="getPropertyIds({{ $languageId }})">
									<label class="custom-control-label" for="language_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Certification</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="certification_status" name="certification_status" wire:model="certificationStatus" wire:change="getPropertyIds({{ $certificationId }})">
									<label class="custom-control-label" for="certification_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="certification_serial_no" name="certification_serial_no" wire:model="certificationSerialNo" wire:change="getPropertyIds({{ $certificationId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="certification_privacy" id="certification_privacy_public" value="0" wire:model="certificationPrivacy" wire:change="getPropertyIds({{ $certificationId }})">
									<label class="custom-control-label" for="certification_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="certification_privacy" id="certification_privacy_private" value="1" wire:model="certificationPrivacy" wire:change="getPropertyIds({{ $certificationId }})">
									<label class="custom-control-label" for="certification_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Course</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="course_status" name="course_status" wire:model="courseStatus" wire:change="getPropertyIds({{ $courseId }})">
									<label class="custom-control-label" for="course_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="course_serial_no" name="course_serial_no" wire:model="courseSerialNo" wire:change="getPropertyIds({{ $courseId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="course_privacy" id="course_privacy_public" value="0" wire:model="coursePrivacy" wire:change="getPropertyIds({{ $courseId }})">
									<label class="custom-control-label" for="course_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="course_privacy" id="course_privacy_private" value="1"  wire:model="coursePrivacy" wire:change="getPropertyIds({{ $courseId }})">
									<label class="custom-control-label" for="course_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Patent</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="patent_status" name="patent_status" wire:model="patentStatus" wire:change="getPropertyIds({{ $patentId }})">
									<label class="custom-control-label" for="patent_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="patent_serial_no" name="patent_serial_no" wire:model="patentSerialNo" wire:change="getPropertyIds({{ $patentId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="patent_privacy" id="patent_privacy_public" value="0" wire:model="patentPrivacy" wire:change="getPropertyIds({{ $patentId }})">
									<label class="custom-control-label" for="patent_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="patent_privacy" id="patent_privacy_private" value="1" wire:model="patentPrivacy" wire:change="getPropertyIds({{ $patentId }})">
									<label class="custom-control-label" for="patent_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Project</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="project_status" name="project_status" wire:model="projectStatus" wire:change="getPropertyIds({{ $projectId }})">
									<label class="custom-control-label" for="project_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="project_serial_no" name="project_serial_no" wire:model="projectSerialNo" wire:change="getPropertyIds({{ $projectId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="project_privacy" id="project_privacy_public" value="0" wire:model="projectPrivacy" wire:change="getPropertyIds({{ $projectId }})">
									<label class="custom-control-label" for="project_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="project_privacy" id="project_privacy_private" value="1" wire:model="projectPrivacy" wire:change="getPropertyIds({{ $projectId }})">
									<label class="custom-control-label" for="project_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Publication</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="publication_status" name="publication_status" wire:model="publicationStatus" wire:change="getPropertyIds({{ $publicationId }})">
									<label class="custom-control-label" for="publication_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="publication_serial_no" name="publication_serial_no" wire:model="publicationSerialNo" wire:change="getPropertyIds({{ $publicationId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="publication_privacy" id="publication_privacy_public" value="0" wire:model="publicationPrivacy" wire:change="getPropertyIds({{ $publicationId }})">
									<label class="custom-control-label" for="publication_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="publication_privacy" id="publication_privacy_private" value="1" wire:model="publicationPrivacy" wire:change="getPropertyIds({{ $publicationId }})">
									<label class="custom-control-label" for="publication_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Organization</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="organization_status" name="organization_status" wire:model="organizationClubStatus" wire:change="getPropertyIds({{ $organizationClubId }})">
									<label class="custom-control-label" for="organization_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="organization_serial_no" name="organization_serial_no" wire:model="organizationClubSerialNo" wire:change="getPropertyIds({{ $organizationClubId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="organization_privacy" id="organization_privacy_public" value="0" wire:model="organizationClubPrivacy" wire:change="getPropertyIds({{ $organizationClubId }})">
									<label class="custom-control-label" for="organization_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="organization_privacy" id="organization_privacy_private" value="1" wire:model="organizationClubPrivacy" wire:change="getPropertyIds({{ $organizationClubId }})">
									<label class="custom-control-label" for="organization_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Award / Honor</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="award_status" name="award_status" wire:model="honorAwardStatus" wire:change="getPropertyIds({{ $honorAwardId }})">
									<label class="custom-control-label" for="award_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="award_serial_no" name="award_serial_no" wire:model="honorAwardSerialNo" wire:change="getPropertyIds({{ $honorAwardId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="award_privacy" id="award_privacy_public" value="0" wire:model="honorAwardPrivacy" wire:change="getPropertyIds({{ $honorAwardId }})">
									<label class="custom-control-label" for="award_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="award_privacy" id="award_privacy_private" value="1" wire:model="honorAwardPrivacy" wire:change="getPropertyIds({{ $honorAwardId }})">
									<label class="custom-control-label" for="award_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Test</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="test_status" name="test_status" wire:model="testStatus" wire:change="getPropertyIds({{ $testId }})">
									<label class="custom-control-label" for="test_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="test_serial_no" name="test_serial_no" wire:model="testSerialNo" wire:change="getPropertyIds({{ $testId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="test_privacy" id="test_privacy_public" value="0" wire:model="testPrivacy" wire:change="getPropertyIds({{ $testId }})">
									<label class="custom-control-label" for="test_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="test_privacy" id="test_privacy_private" value="1" wire:model="testPrivacy" wire:change="getPropertyIds({{ $testId }})">
									<label class="custom-control-label" for="test_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Social Work</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="socialWork_status" name="socialWork_status" wire:model="volanteerSocialworkStatus" wire:change="getPropertyIds({{ $volanteerSocialworkId }})">
									<label class="custom-control-label" for="socialWork_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="socialWork_serial_no" name="socialWork_serial_no" wire:model="volanteerSocialworkSerialNo" wire:change="getPropertyIds({{ $volanteerSocialworkId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="socialWork_privacy" id="socialWork_privacy_public" value="0" wire:model="volanteerSocialworkPrivacy" wire:change="getPropertyIds({{ $volanteerSocialworkId }})">
									<label class="custom-control-label" for="socialWork_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="socialWork_privacy" id="socialWork_privacy_private" value="1" wire:model="volanteerSocialworkPrivacy" wire:change="getPropertyIds({{ $volanteerSocialworkId }})">
									<label class="custom-control-label" for="socialWork_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Extra Curriculumn</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="extraCurriculumn_status" name="extraCurriculumn_status" wire:model="extraCurriculumnStatus" wire:change="getPropertyIds({{ $extraCurriculumnId }})">
									<label class="custom-control-label" for="extraCurriculumn_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="extraCurriculumn_serial_no" name="extraCurriculumn_serial_no" wire:model="extraCurriculumnSerialNo" wire:change="getPropertyIds({{ $extraCurriculumnId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="extraCurriculumn_privacy" id="extraCurriculumn_privacy_public" value="0" wire:model="extraCurriculumnPrivacy" wire:change="getPropertyIds({{ $extraCurriculumnId }})">
									<label class="custom-control-label" for="extraCurriculumn_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="extraCurriculumn_privacy" id="extraCurriculumn_privacy_private" value="1" wire:model="extraCurriculumnPrivacy" wire:change="getPropertyIds({{ $extraCurriculumnId }})">
									<label class="custom-control-label" for="extraCurriculumn_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

			<li class="list-group-item">
				<strong class="property-name">Recommandation</strong>

				<span class="property-box">
					<div class="row">
						
						<div class="col-sm-3">
							<span class="property-status">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="recommandation_status" name="recommandation_status" wire:model="recommandationStatus" wire:change="getPropertyIds({{ $recommandationId }})">
									<label class="custom-control-label" for="recommandation_status">Active</label>
								</div>
							</span>
						</div>

						<div class="col-sm-3">
							<span class="property-serial-no">
								<input type="number" class="form-control" id="recommandation_serial_no" name="recommandation_serial_no" wire:model="recommandationSerialNo" wire:change="getPropertyIds({{ $recommandationId }})">
							</span>
						</div>

						<div class="col-sm-6">
							<span class="property-privacy">
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="recommandation_privacy" id="recommandation_privacy_public" value="0" wire:model="recommandationPrivacy" wire:change="getPropertyIds({{ $recommandationId }})">
									<label class="custom-control-label" for="recommandation_privacy_public">Public</label>
								</span>
								<span class="custom-control custom-radio property-radio-box">
									<input type="radio" class="custom-control-input" name="recommandation_privacy" id="recommandation_privacy_private" value="1" wire:model="recommandationPrivacy" wire:change="getPropertyIds({{ $recommandationId }})">
									<label class="custom-control-label" for="recommandation_privacy_private">Private</label>
								</span>
							</span>
						</div>

					</div>
				</span>
			</li>

		</ul>
	</div>

	<!-- Modal footer -->
	<div class="modal-footer">
	  	<button wire:click="update()" type="button" class="btn btn-success" data-dismiss="modal">Save & Close</button>
	</div>

</div>
