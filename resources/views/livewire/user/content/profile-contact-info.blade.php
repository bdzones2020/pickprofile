<div class="mt-4">
    <h3 class="h5 mb-2 text-secondary text-uppercase fw400">
        Contact Information
        <button wire:click="editContactInfo()" class="btn btn-xs btn-dark pull-right"><i class="fa fa-edit"></i></button>
    </h3>

    @if($contactInfoInput)
        <!-- Experience Inputs -->
        @livewire('user.content.inputs.contact-info-inputs')
        <!-- End Experience Inputs -->
    @endif

    <hr class="mt-1 mb-1" />
    <div class="fw400 mb-1">
        <strong><i class="fa fa-mobile"></i></strong>
        <a href="tel:{{ $profile->contact_no }}">{{ $profile->contact_no }}</a>
    </div>

    <div class="fw400 mb-1">
        <strong><i class="fa fa-envelope"></i></strong>
        <a href="mailto:{{ $profile->contact_email }}">{{ $profile->contact_email }}</a>
    </div>

    <div class="fw400 mb-1">
        <address class="mb-0">
            <strong><i class="fa fa-home"></i></strong>
            {{ $profile->present_address }}
        </address>
    </div>
</div>