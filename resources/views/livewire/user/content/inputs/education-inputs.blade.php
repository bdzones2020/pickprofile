<div class="card mb-3">
	<div id="educationInputs" class="card-body">
		<div class="row form-group">
			<div class="col-12">
				<h5 class="font-weight-bold">
    				<span class="mode">{{ $inputMode ? $inputMode : '' }}</span> Education
    				<span class="submitBox">
						<button wire:click="cancel()" type="button" class="btn btn-sm btn-outline-danger">Cancel</button>
					</span>
    			</h5>
			</div>
		</div>
		<hr>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="company" class="control-label">School / University</label>
            </div>
            <div class="col-sm-8">
                <input wire:model="school" id="school" type="text" class="form-control" name="school" placeholder="School / University" value="" required="" autofocus="true">
            </div>
        </div>
        <input type="hidden" wire:model="selectedId">
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="degree" class="control-label">Degree</label>
            </div>
            <div class="col-sm-8">
                <input wire:model="degree" id="degree" type="text" class="form-control" name="degree" placeholder="Degree" value="" required="">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="field_of_study" class="control-label">Field of Study</label>
            </div>
            <div class="col-sm-8">
                <input wire:model="fieldOfStudy" id="field_of_study" type="text" class="form-control" name="field_of_study" placeholder="Field of Study" value="" required="">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="started" class="control-label">Started</label>
            </div>
            <div class="col-sm-8">
                <select wire:model="started" name="started" id="started" class="form-control col-sm-4" required="">
                    @if($years && count($years) > 0)
                        @foreach($years as $year)
                            @if($started && $started == $year)
                            <option value="{{ $year }}" selected="">{{ $year }}</option>
                            @else
                            <option value="{{ $year }}">{{ $year }}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="ended" class="control-label">Ended</label>
            </div>
            <div class="col-sm-8">
                <select wire:model="ended" name="ended" id="ended" class="form-control col-sm-4 d-inline">
                    @if($years && count($years) > 0)
                        @foreach($years as $year)
                        <option value="{{ $year }}" selected="{{ $year == $ended ? true : false }}">{{ $year }}</option>
                        @endforeach
                    @endif
                </select>
                <span class="d-inline ml-2">
                	<div class="custom-control custom-switch d-inline">
						<input type="checkbox" class="custom-control-input" id="stillStudying" name="stillStudying" value="1" wire:model="stillStudying">
						<label class="custom-control-label" for="stillStudying">Still Studying</label>
					</div>
                </span>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="grade" class="control-label">Grade / Result</label>
            </div>
            <div class="col-sm-8">
                <input wire:model="grade" id="grade" type="text" class="form-control" name="grade" placeholder="Grade / Result" value="" required="">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="grade_point" class="control-label">Grade Point / Marks</label>
            </div>
            <div class="col-sm-8">
                <input wire:model="gradePoint" id="grade_point" type="text" class="form-control" name="grade_point" placeholder="Grade Point / Marks" value="" required="">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="details" class="control-label">Details</label>
            </div>
            <div class="col-sm-8">
                <textarea wire:model="details" class="form-control" name="details" id="details" cols="1" rows="2" placeholder="activity details ..."></textarea>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="serial_no" class="control-label">Serial No</label>
            </div>
            <div class="col-sm-8">
                <input wire:model="serialNo" id="serial_no" type="number" class="form-control" name="serial_no" placeholder="0" value="">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="privacy" class="control-label">Privacy</label>
            </div>
            <div class="col-sm-8">
                <span class="property-privacy">
					<span class="custom-control custom-radio property-radio-box">
						<input type="radio" class="custom-control-input" name="privacy" value="0" id="privacy_public" wire:model="privacy">
						<label class="custom-control-label" for="privacy_public">Public</label>
					</span>
					<span class="custom-control custom-radio property-radio-box">
						<input type="radio" class="custom-control-input" name="privacy" value="1" id="privacy_private" wire:model="privacy">
						<label class="custom-control-label" for="privacy_private">Private</label>
					</span>
				</span>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4">
                <label for="privacy" class="control-label">Status</label>
            </div>
            <div class="col-sm-8">
                <span class="property-status">
					<div class="custom-control custom-switch">
						<input type="checkbox" class="custom-control-input" id="status" name="status" value="1" wire:model="status">
						<label class="custom-control-label" for="status">Active</label>
					</div>
				</span>
				<span class="submitBox">
					<button wire:click="save()" type="button" class="btn btn-sm btn-success">Save & Close</button>
				</span>
            </div>
        </div>
    </div>
</div>