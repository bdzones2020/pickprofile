<div class="card mb-3">
	<div id="contactInputs" class="card-body contactInputs">
        <div class="row form-group">
            <div class="col-12">
                <h5 class="font-weight-bold">
                    <span class="mode">Edit Contact Infos</span>
                    <span class="submitBox">
                        <button wire:click="cancel()" type="button" class="btn btn-sm btn-outline-danger">Cancel</button>
                    </span>
                </h5>
            </div>
        </div>
        <hr>
        <div class="row form-group">
            <div class="col-sm-12">
                <label for="contact_no" class="control-label">Contact No</label>
                <input wire:model="contactNo" id="contact_no" type="tel" class="form-control quantity_class" maxlength="15" name="contact_no" value="">
                <span id="contact-valid-msg" class="d-none text-success">✓ Valid</span>
                <span id="contact-error-msg" class="d-none text-danger"></span>
                <input type="hidden" id="countryCode" name="countryCode" value="">
            </div>
        </div>

        <div class="row form-group">
            <div class="col-sm-12">
                <label for="contact_email" class="control-label">Contact Email</label>
                <input wire:model="contactEmail" type="email" class="form-control contact_email" id="contact_email" name="contact_email" value="" placeholder="account@email.com">
                <span id="email-valid-msg" class="hide text-success">✓ Valid</span>
                <span id="email-error-msg" class="hide text-danger"> X Invalid</span>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-sm-12">
                <label for="present_address" class="control-label">Present Address</label>

                <textarea wire:model="presentAddress" name="present_address" id="present_address" cols="1" rows="2" class="form-control" placeholder="present address"></textarea>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-sm-12">
                <span class="submitBox">
                    <button wire:click="update()" type="button" class="btn btn-sm btn-success">Save & Close</button>
                </span>
            </div>
        </div>
    </div>
</div>