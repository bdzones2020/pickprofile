
<hr class="mt-2 mb-2">
<div class="">
    <section class="mb-1 pb-1">
        <h3 class="h5 text-dark text-uppercase fw400">
            Work Experience
            @if($experiences && count($experiences) > 2)
            <button data-toggle="collapse" data-target="#restExperience" class=" btn btn-light btn-xs mr-2">+{{ count($experiences) - 2 }} More</button>
            @endif
            <button wire:model="createBtn" wire:click="createExperience()" class="btn btn-xs btn-primary pull-right"><i class="fa fa-plus"></i></button>
            <div>
                <small class="totalYear fw400 text-dark">4 Years 2 months</small>
            </div>
        </h3>

        @if($experienceInput)
	        <!-- Experience Inputs -->
		    @livewire('user.content.inputs.experience-inputs')
	        <!-- End Experience Inputs -->
        @endif

        @if($experiences && count($experiences) > 0)

	        <div class="work-experience">

	        	@foreach($experiences as $e => $experience)

	        		@if(count($experiences) > 2 && $e >= 2)

                    <div id="restExperience" class="collapse">

                    @endif

	        		<div class="work mb-2">
		                <span class="h6 d-block text-primary mb-1">
		                    {{ $experience->designation }}
		                    <span class="text-secondary">
		                        ({{ $experience->joined }}{{ $experience->left ? ' - ' .$experience->left : '' }})
		                    </span>
		                    <div class="pull-right btn-group">
		                    	<button wire:click="editExperience({{ $experience->id }})" class="btn btn-xs btn-outline-success pull-right"><i class="fa fa-edit"></i></button>
			                    <button wire:click="deleteExperience({{ $experience->id }})" class="btn btn-xs btn-outline-danger pull-right"><i class="fa fa-trash"></i></button>
		                    </div>
		                </span>
		                <div class="text-secondary fw400">
		                    <strong>
		                        {{ $experience->company }}
		                    </strong>,
		                    <span class="fw400">
		                        {{ $experience->location }}
		                    </span>
		                </div>
		            </div>

		            @if(count($experiences) > 2 && $e >= 2)

                    </div>

                    @endif

	        	@endforeach

	        </div>

        @endif
        
    </section>
</div>