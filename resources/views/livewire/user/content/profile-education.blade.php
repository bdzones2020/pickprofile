
<hr class="mt-2 mb-2" />
<div class="">
    <section class="mb-1 pb-1">
        <h3 class="h5 text-dark text-uppercase fw400">
            Education
            @if($educations && count($educations) > 2)
            <button data-toggle="collapse" data-target="#restEducation" class="btn btn-light btn-xs">+{{ count($educations) - 2 }} More</button>
            @endif
            <button wire:model="createBtn" wire:click="createEducation()" class="btn btn-xs btn-primary pull-right"><i class="fa fa-plus"></i></button>
        </h3>

        @if($educationInput)
            <!-- Experience Inputs -->
            @livewire('user.content.inputs.education-inputs')
            <!-- End Experience Inputs -->
        @endif

        @if($educations && count($educations) > 0)
        <div class="work-experience">

            @foreach($educations as $ed => $education)

                @if(count($educations) > 2 && $ed >= 2)

                    <div id="restEducation" class="collapse">

                @endif

                <div class="work mb-2">
                    <span class="h6 d-block text-secondary mb-1">
                        <span class="text-primary">
                            {{ $education->degree }}
                        </span>
                        <span class="text-secondary">
                            {{ $education->field_of_study }}
                        </span>
                        ,
                        <span>
                            {{ $education->grade }} <small>({{ $education->grade_point }})</small>
                        </span>
                        <div class="pull-right btn-group">
                            <button wire:click="editEducation({{ $education->id }})" class="btn btn-xs btn-outline-success pull-right"><i class="fa fa-edit"></i></button>
                            <button wire:click="deleteEducation({{ $education->id }})" class="btn btn-xs btn-outline-danger pull-right"><i class="fa fa-trash"></i></button>
                        </div>
                    </span>
                    <div class="text-secondary fw400">
                        <strong>
                            {{ $education->school }}
                        </strong>
                        ,
                        <span class="fw400">
                            ({{ $education->started }}{{ $education->ended ? ' - ' .$education->ended : '' }})
                        </span>
                    </div>
                </div>

                @if(count($educations) > 2 && $ed >= 2)

                    </div>

                @endif

            @endforeach

        </div>
        @endif

    </section>
</div>