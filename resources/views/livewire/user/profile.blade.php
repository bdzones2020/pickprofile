<div class="row">
    <div class="container-fluid">
        <h1 class="page-heading">
            Profile of {{ Auth::user()->user_name }}
            <button wire:click="editProfileSettings()" id="profileSettingBtn" class="pull-right btn btn-sm btn-dark mt-1" data-toggle="tooltip" title="Profile Settings" data-placement="left"><i class="fa fa-gear"></i></button>
        </h1>

        <hr class="page-heading-hr" />
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-md-4">
                <img class="w-100" src="{{ asset('img/profile-dummy.jpg') }}" />

                <div class="infoDivLg">
                    @if($profileContactInfoStatus)
                        
                        @include('livewire.user.content.profile-contact-info')

                    @endif

                    <div class="mt-3">
                        <h3 class="h5 mb-2 text-secondary text-uppercase fw400">Basic Information</h3>
                        <hr class="mt-1 mb-1" />
                        <div class="fw400 mb-1"><strong>Gender</strong> MALE</div>

                        @if($profileDobStatus)

                            <div wire:model="" class="fw400 mb-1"><strong>Date of Birth</strong> 24th May, 1990</div>

                        @endif
                    </div>

                    @if($profileInterestStatus)
                        <div class="mt-3">
                            <h3 class="h5 mb-2 text-secondary text-uppercase fw400">Interests</h3>
                            <hr class="mt-1 mb-1" />
                            <div class="fw400 mb-1">
                                <strong>
                                    Traveling
                                </strong>
                                ,
                                <strong>
                                    Photography
                                </strong>
                                ,
                                <strong>
                                    Sports
                                </strong>
                                ,
                                <strong>
                                    Music
                                </strong>
                            </div>
                        </div>
                    @endif

                    @if($profileSocialNetworkStatus)
                        <div class="mt-3">
                            <h3 class="h5 mb-2 text-secondary text-uppercase fw400">Social & Professional Networks</h3>
                            <hr class="mt-1 mb-1" />
                            <div class="social-network-bar fw400 mb-1">
                                <div class="row">
                                    <div class="col-3">
                                        <strong>
                                            Gitlab
                                        </strong>
                                    </div>
                                    <div class="col-9">
                                        <a href="https://gitlab.com/mosharof29">https://gitlab.com/mosharof29</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-3">
                                        <strong>
                                            Freelancer
                                        </strong>
                                    </div>
                                    <div class="col-9">
                                        <a href="https://www.freelancer.com/">https://www.freelancer.com/</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-3">
                                        <strong>
                                            Facebook
                                        </strong>
                                    </div>
                                    <div class="col-9">
                                        <a href="https://www.facebook.com/mosharof.hossen1">https://www.facebook.com/mosharof.hossen1</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-3">
                                        <strong>
                                            Instagram
                                        </strong>
                                    </div>
                                    <div class="col-9">
                                        <a href="https://www.instagram.com/sonzu307/">https://www.instagram.com/sonzu307/</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="titleDivSm">
                    <div class="d-flex align-items-center">
                        <h2 class="font-weight-bold m-0">
                            Harry J. Hamilton
                        </h2>
                        <address class="m-0 pt-2 pl-0 pl-md-4 font-weight-light text-secondary">
                            <i class="fa fa-map-marker"></i>
                            Garden City, NY
                        </address>
                    </div>
                    <p class="h5 text-info mt-2 d-block fw400">
                        Full-Stack Programmer
                        <small class="ml-3 text-secondary">Efficiency Points: 275 </small>
                    </p>
                    <div class="skill-bar mt-2 text-info fw400">
                        <strong class="h5 text-secondary font-weight-bold mb-1 mr-1">Skills</strong>
                        <span>HTML5</span>
                        <span>CSS3</span>
                        <span>JAVASCRIPT</span>
                        <span>PHP</span>
                        <span>MYSQL</span>
                    </div>

                    <div class="available-bar pb-1 mt-2 text-info fw400">
                        <strong class="h5 text-secondary font-weight-bold mb-1 mr-1">Available For</strong>
                        <span><i class="fa fa-check-square"></i> Full Time</span>
                        <span><i class="fa fa-check-square"></i> Part Time</span>
                        <span><i class="fa fa-check-square"></i> Contractual</span>
                        <span><i class="fa fa-check-square"></i> Casual / Seassional</span>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="titleDivLg">
                    <div class="d-flex align-items-center">
                        <h2 class="font-weight-bold m-0">
                            Harry J. Hamilton
                        </h2>
                        <address class="m-0 pt-2 pl-0 pl-md-4 font-weight-light text-secondary fw400">
                            <i class="fa fa-map-marker"></i>
                            Garden City, NY
                        </address>
                    </div>
                    <p class="h5 text-info mt-2 d-block fw400">
                        Full-Stack Programmer
                        <small class="ml-3 text-secondary">Efficiency Points: 275 </small>
                    </p>
                    <div class="skill-bar mt-2 text-info fw400">
                        <strong class="h5 text-secondary font-weight-bold mb-1 mr-1">Skills</strong>
                        <span>HTML5</span>
                        <span>CSS3</span>
                        <span>JAVASCRIPT</span>
                        <span>PHP</span>
                        <span>MYSQL</span>
                    </div>

                    <div class="available-bar pb-1 mt-2 text-info fw400">
                        <strong class="h5 text-secondary font-weight-bold mb-1 mr-1">Available For</strong>
                        <span><i class="fa fa-check-square"></i> Full Time</span>
                        <span><i class="fa fa-check-square"></i> Part Time</span>
                        <span><i class="fa fa-check-square"></i> Contractual</span>
                        <span><i class="fa fa-check-square"></i> Casual / Seassional</span>
                    </div>
                </div>

                @if($profileSettings && count($profileSettings) > 0)

                    @foreach($profileSettings as $pp => $profileSetting)

                        @if($profileSetting->property->name == 'EXPERIENCE' && $profileSetting->status)

                            @include('livewire.user.content.profile-experience')

                        @endif

                        @if($profileSetting->property->name == 'EDUCATION' && $profileSetting->status)

                            @include('livewire.user.content.profile-education')

                        @endif

                    @endforeach

                

                <!-- {{-- start Experience --}} -->
                <!-- <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">
                            Work Experience
                            <button data-toggle="collapse" data-target="#restExperience" class="btn btn-light btn-xs mr-2">+1 More</button>
                            <button class="btn btn-xs btn-primary pull-right"><i class="fa fa-plus"></i></button>
                            <div>
                                <small class="totalYear fw400">4 Years 2 months</small>
                            </div>
                        </h3>
                        <div class="work-experience">
                            <div class="work mb-2">
                                <span class="h6 d-block text-info mb-1">
                                    Front End Developer
                                    <span class="text-secondary">
                                        (Jan 2015 - Dec 2017)
                                    </span>
                                    <button class="btn btn-xs btn-dark pull-right"><i class="fa fa-edit"></i></button>
                                </span>
                                <div class="text-secondary fw400">
                                    <strong>
                                        Prodesign Inc
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        Southern Street Floral Park, NY 11001, USA
                                    </span>
                                </div>
                            </div>

                            <div class="work mb-2">
                                <span class="h6 d-block text-info mb-1">
                                    Senior Programmer
                                    <span class="text-secondary">
                                        (Apr 2012 - Dec 2014)
                                    </span>
                                </span>
                                <div class="text-secondary fw400">
                                    <strong>
                                        Blue Tech
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        George Avenue Mobile, AL 36608, USA
                                    </span>
                                </div>
                            </div>

                            <div id="restExperience" class="collapse">
                                <div class="work mb-2">
                                    <span class="h6 d-block text-info mb-1">
                                        Assistant Programmer
                                        <span class="text-secondary">
                                            (Jul 2009 - Jan 2012)
                                        </span>
                                    </span>
                                    <div class="text-secondary fw400">
                                        <strong>
                                            Binarymen Pvt. Ltd.
                                        </strong>
                                        ,
                                        <span class="fw400">
                                            South Region Hill Track, Morana, AZ 36608, USA
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div> -->
                <!-- {{-- end Experience --}} -->

                <!-- {{-- start Education --}} -->
                <!-- <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">
                            Education
                            <button data-toggle="collapse" data-target="#restEducation" class="btn btn-light btn-xs">+2 More</button>
                        </h3>
                        <div class="work-experience">
                            <div class="work mb-2">
                                <span class="h6 d-block text-secondary mb-1">
                                    Masters of Arts (M.A.)
                                    <span class="text-secondary">
                                        English Literature
                                    </span>
                                    ,
                                    <span>
                                        2nd Class
                                    </span>
                                </span>
                                <div class="text-secondary fw400">
                                    <strong>
                                        Dhaka University
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        (2009 - 2010)
                                    </span>
                                </div>
                            </div>
                
                            <div class="work mb-2">
                                <span class="h6 d-block text-secondary mb-1">
                                    Bachelor of Arts (B.A.)
                                    <span class="text-secondary">
                                        English Literature
                                    </span>
                                    ,
                                    <span>
                                        2nd Class
                                    </span>
                                </span>
                                <div class="text-secondary fw400">
                                    <strong>
                                        Dhaka University
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        (2004 - 2008)
                                    </span>
                                </div>
                            </div>
                
                            <div id="restEducation" class="collapse">
                                <div class="work mb-2">
                                    <span class="h6 d-block text-secondary mb-1">
                                        Higher Secondary Certificate (HSC)
                                        <span class="text-secondary">
                                            Science
                                        </span>
                                        ,
                                        <span>
                                            B (3.40)
                                        </span>
                                    </span>
                                    <div class="text-secondary fw400">
                                        <strong>
                                            BCIC College
                                        </strong>
                                        ,
                                        <span class="fw400">
                                            (2001 - 2003)
                                        </span>
                                    </div>
                                </div>
                
                                <div class="work mb-2">
                                    <span class="h6 d-block text-secondary mb-1">
                                        Secondary School Certificate (SSC)
                                        <span class="text-secondary">
                                            Science
                                        </span>
                                        ,
                                        <span>
                                            A (4.13)
                                        </span>
                                    </span>
                                    <div class="text-secondary fw400">
                                        <strong>
                                            Mirpur Govt. High School
                                        </strong>
                                        ,
                                        <span class="fw400">
                                            (1995 - 2001)
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div> -->
                <!-- {{-- end Education --}} -->

                <!-- {{-- start Certification --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">
                            Certification
                            <button data-toggle="collapse" data-target="#restCertification" class="btn btn-light btn-xs">+1 More</button>
                        </h3>
                        <div class="work-experience">
                            <div class="work mb-2">
                                <span class="h6 d-block text-secondary mb-1">
                                    Oracle Database 10g Administrator Certified Professional
                                    <small>by</small>
                                    <span class="text-secondary">
                                        Oracle
                                    </span>
                                    ,
                                    <span>
                                        August 11, 2014
                                    </span>
                                </span>
                            </div>

                            <div class="work mb-2">
                                <span class="h6 d-block text-secondary mb-1">
                                    Oracle PL/SQL Developer Certified Associate
                                    <small>by</small>
                                    <span class="text-secondary">
                                        Oracle
                                    </span>
                                    ,
                                    <span>
                                        January 24, 2014
                                    </span>
                                </span>
                            </div>

                            <div id="restCertification" class="collapse">
                                <div class="work mb-2">
                                    <span class="h6 d-block text-secondary mb-1">
                                        Oracle SQL Developer Certified Master
                                        <small>by</small>
                                        <span class="text-secondary">
                                            Oracle
                                        </span>
                                        ,
                                        <span>
                                            May 07, 2013
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Certification --}} -->

                <!-- {{-- start Language --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">
                            Language
                            <button data-toggle="collapse" data-target="#restLanguage" class="btn btn-light btn-xs">+1 More</button>
                        </h3>
                        <div class="work-experience">
                            <div class="work mb-2">
                                <span class="d-block text-secondary mb-1">
                                    <strong>
                                        Bengoli
                                    </strong>
                                    ,
                                    <span class="text-secondary">
                                        Lavel - Native
                                    </span>
                                    , <span> <i class="fa fa-check-square"></i> Speaking </span>, <span> <i class="fa fa-check-square"></i> Listening </span>, <span> <i class="fa fa-check-square"></i> Writing </span>,
                                    <span> <i class="fa fa-check-square"></i> Reading </span>
                                </span>
                            </div>

                            <div class="work mb-2">
                                <span class="d-block text-secondary mb-1">
                                    <strong>
                                        English
                                    </strong>
                                    ,
                                    <span class="text-secondary">
                                        Lavel - Proficient
                                    </span>
                                    , <span> <i class="fa fa-check-square"></i> Speaking </span>, <span> <i class="fa fa-check-square"></i> Listening </span>, <span> <i class="fa fa-check-square"></i> Writing </span>,
                                    <span> <i class="fa fa-check-square"></i> Reading </span>
                                </span>
                            </div>

                            <div id="restLanguage" class="collapse">
                                <div class="work mb-2">
                                    <span class="d-block text-secondary mb-1">
                                        <strong>
                                            Hindi
                                        </strong>
                                        ,
                                        <span class="text-secondary">
                                            Lavel - Proficient
                                        </span>
                                        , <span> <i class="fa fa-check-square"></i> Speaking </span>,
                                        <span> <i class="fa fa-check-square"></i> Listening </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Language --}} -->

                <!-- {{-- start Course --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">
                            Courses
                            <button data-toggle="collapse" data-target="#restCourse" class="btn btn-light btn-xs">+1 More</button>
                        </h3>
                        <div class="work-experience">
                            <div class="work mb-2">
                                <span class="h6 d-block text-secondary mb-1">
                                    Database Designing and Developement
                                    <small>in</small>
                                    <span class="text-secondary">
                                        IBCS-Primax Bangladesh
                                    </span>
                                    ,
                                    <span>
                                        February 2013 - November 2013
                                    </span>
                                </span>
                            </div>

                            <div class="work mb-2">
                                <span class="h6 d-block text-secondary mb-1">
                                    Commom Ground Journalism
                                    <small>in</small>
                                    <span class="text-secondary">
                                        Bangladesh Sangbad Sangstha
                                    </span>
                                    ,
                                    <span>
                                        July 2008 - October 2008
                                    </span>
                                </span>
                            </div>

                            <div id="restCourse" class="collapse">
                                <div class="work mb-2">
                                    <span class="h6 d-block text-secondary mb-1">
                                        Professional Basic in Photography
                                        <small>in</small>
                                        <span class="text-secondary">
                                            South Asian Photography Institute
                                        </span>
                                        ,
                                        <span>
                                            January 2007 - April 2007
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Course --}} -->

                <!-- {{-- start Patent --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">Patent</h3>
                        <div class="work-experience">
                            <div class="work mb-2">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Designer of the Logo of Biman Bangladesh Airlines
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        (Total Members: 3)
                                    </span>
                                </div>
                                <div class="text-secondary fw400">
                                    <strong>
                                        Patent Administration, Government of Bangladesh
                                    </strong>
                                    ,
                                    <strong>
                                        Issue Date: 22 March, 2006
                                    </strong>
                                    ,
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Patent --}} -->

                <!-- {{-- start Publication --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">Publication</h3>
                        <div class="work-experience">
                            <div class="work mb-2">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Adventure of Master Da
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        Brothers Publishers
                                    </span>
                                    ,
                                    <span class="fw400">
                                        15 July, 2011
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Publication --}} -->

                <!-- {{-- start Organization --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">Organization / Club</h3>
                        <div class="work-experience">
                            <div class="work mb-1">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Dhaka University Tourist Society
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        General Member
                                    </span>
                                    ,
                                    <span class="fw400">
                                        (Active)
                                    </span>
                                </div>
                            </div>

                            <div class="work mb-1">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Hashimukh
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        Secretary
                                    </span>
                                    ,
                                    <span class="fw400">
                                        (Former)
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Organization --}} -->

                <!-- {{-- start Honor/Award --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">Honor / Award</h3>
                        <div class="work-experience">
                            <div class="work mb-1">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Chess Master of the Year 2002
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        Chess Board of Bangladesh
                                    </span>
                                </div>
                            </div>

                            <div class="work mb-1">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Nature Paint Compatition 2009
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        First Position
                                    </span>
                                    ,
                                    <span class="fw400">
                                        15 October, 2005
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Honor/Award --}} -->

                <!-- {{-- start Test --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">Test</h3>
                        <div class="work-experience">
                            <div class="work mb-1">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Basic IQ Test
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        Score: 6.5 / 10
                                    </span>
                                    ,
                                    <span class="fw400">
                                        27 March, 2012
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Test --}} -->

                <!-- {{-- start Project --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">Project</h3>
                        <div class="work-experience">
                            <div class="work mb-1">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Dairy Farm Project
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        A project on implemantation hi-tech farming of dairy.
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Project --}} -->

                <!-- {{-- start Volunteer/Social Work --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">Volunteer / Social Work</h3>
                        <div class="work-experience">
                            <div class="work mb-1">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Scout
                                    </strong>
                                    ,
                                    <span class="fw400">
                                        Social welfair activities
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Volunteer/Social Work --}} -->

                <!-- {{-- start Extra Curriculumn --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">Extra Curriculumn</h3>
                        <div class="work-experience">
                            <div class="work mb-1">
                                <div class="text-secondary fw400">
                                    <strong>
                                        Painting
                                    </strong>
                                    ,
                                    <strong>
                                        Singing
                                    </strong>
                                    ,
                                    <strong>
                                        Cricket
                                    </strong>
                                    ,
                                    <strong>
                                        Football
                                    </strong>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Extra Curriculumn --}} -->

                <!-- {{-- start Recommandation --}} -->
                <hr class="mt-2 mb-2" />
                <div class="">
                    <section class="mb-1 pb-1">
                        <h3 class="h5 text-dark text-uppercase fw400">Recommandation</h3>
                        <div class="work-experience">
                            <div class="work mb-1 pb-1">
                                <div class="text-secondary fw400">
                                    <q>He is an honest and responsible person in any way.</q>
                                    <div>- <strong>David Copperfield, General Manager, SouthTech IT</strong></div>
                                </div>
                            </div>

                            <div class="work mb-1 pb-1">
                                <div class="text-secondary fw400">
                                    <q>Very mush efficient in his track. I am sure he will shines much more in his career.</q>
                                    <div>- <strong>Jassy Moore, Chief Programmer, BITM</strong></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- {{-- end Recommandation --}} -->

                @endif

                <div class="infoDivSm">

                    @if($profileContactInfoStatus)
                        
                        @include('livewire.user.content.profile-contact-info')
                        
                    @endif

                    <div class="mt-3">
                        <h3 class="h5 mb-2 text-secondary text-uppercase fw400">Basic Information</h3>
                        <hr class="mt-1 mb-1" />
                        <div class="fw400 mb-1"><strong>Gender</strong> MALE</div>

                        @if($profileDobStatus)
                            <div class="fw400 mb-1"><strong>Date of Birth</strong> 24th May, 1990</div>
                        @endif
                    </div>

                    @if($profileInterestStatus)
                        <div class="mt-3">
                            <h3 class="h5 mb-2 text-secondary text-uppercase fw400">Interests</h3>
                            <hr class="mt-1 mb-1" />
                            <div class="fw400 mb-1">
                                <strong>
                                    Traveling
                                </strong>
                                ,
                                <strong>
                                    Photography
                                </strong>
                                ,
                                <strong>
                                    Sports
                                </strong>
                                ,
                                <strong>
                                    Music
                                </strong>
                            </div>
                        </div>
                    @endif

                    @if($profileSocialNetworkStatus)
                        <div class="mt-3">
                            <h3 class="h5 mb-2 text-secondary text-uppercase fw400">Social & Professional Networks</h3>
                            <hr class="mt-1 mb-1" />
                            <div class="social-network-bar fw400 mb-1">
                                <div class="row">
                                    <div class="col-3">
                                        <strong>
                                            Gitlab
                                        </strong>
                                    </div>
                                    <div class="col-9">
                                        <a href="https://gitlab.com/mosharof29">https://gitlab.com/mosharof29</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-3">
                                        <strong>
                                            Freelancer
                                        </strong>
                                    </div>
                                    <div class="col-9">
                                        <a href="https://www.freelancer.com/">https://www.freelancer.com/</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-3">
                                        <strong>
                                            Facebook
                                        </strong>
                                    </div>
                                    <div class="col-9">
                                        <a href="https://www.facebook.com/mosharof.hossen1">https://www.facebook.com/mosharof.hossen1</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-3">
                                        <strong>
                                            Instagram
                                        </strong>
                                    </div>
                                    <div class="col-9">
                                        <a href="https://www.instagram.com/sonzu307/">https://www.instagram.com/sonzu307/</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="profileSettingsModal">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        @livewire('user.inc.profile-settings')
    </div>
</div>
