
<div>
    <div class="form-group">
        <label for="name">Name</label>
        <input id="name" type="text" wire:model="name" class="form-control input-sm"  placeholder="Name">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input id="email" type="email" class="form-control input-sm" placeholder="Email" wire:model="email">
    </div>
    <div class="form-group">
        <label for="contact_no">Contact No</label>
        <input id="contact_no" type="tel" class="form-control input-sm" placeholder="Contact No" wire:model="contact_no">
    </div>
    <div class="form-group">
        <label for="message">Message</label>
        <textarea name="message" id="message" cols="1" rows="2" class="form-control" wire:model="message"></textarea>
    </div>
    <div class="form-group">
        @if($image)
        <img src="{{ $image }}" alt="" style="height: 160px;width: auto;">
        @endif
        <label for="image">Image</label>
        <input type="file" id="image" wire:change="$emit('imageChosen')">
    </div>
    <div class="form-group">
        <label for="image">File Size</label>
        <input type="text" wire:model="fileSize">
    </div>
    <button wire:click="store()" class="btn btn-primary">Submit</button>
</div>
