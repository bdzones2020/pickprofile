
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                	Message Page <strong wire:poll.5000ms>Time (Updated on every 5 sec): {{ now() }}</strong>

                	@if($showMode)
                	<button wire:click="create()" class="btn btn-sm btn-outline-primary py-0" style="float: right;">Create</button>
                	@endif

                	@if($closeMode)
                	<button wire:click="close()" class="btn btn-sm btn-outline-primary py-0" style="float: right;">Close</button>
                	@endif
                </div>

                <div class="card-header">
                	Live Search : <input wire:model="search" type="text" placeholder="search here" class="form-control">
                </div>

                <div class="card-body">

                	<div wire:loading wire:target="search">
				        <div class="spinner-border" role="status">
							<span class="sr-only">Loading...</span>
						</div>
				    </div>

                	@if (count($errors) > 0)
				        <div class="alert alert-danger">
				            <a href="#" class="close" data-dismiss="alert">&times;</a>
				            <strong>Sorry!</strong> invalid input.<br><br>
				            <ul style="list-style-type:none;">
				                @foreach ($errors->all() as $error)
				                    <li>{{ $error }}</li>
				                @endforeach
				            </ul>
				        </div>
				    @endif

                	@if($updateMode)
				        @include('livewire.post.update')
				    @endif

				    @if($createMode)
				        @include('livewire.post.create')
				    @endif
                    
                    <div class="">

					    <table class="table table-striped" style="margin-top:20px;">
					        <tr>
					            <td>NO</td>
					            <td>Name</td>
					            <td>Email</td>
					            <td>Contact No</td>
					            <td>Message</td>
					            <td>Image</td>
					            <td>ACTION</td>
					        </tr>

					        @foreach($data as $row)
					            <tr>
					                <td>{{$loop->index + 1}}</td>
					                <td>{{$row->name}}</td>
					                <td>{{$row->email}}</td>
					                <td>{{$row->contact_no}}</td>
					                <td>{{$row->message}}</td>
					                @if($row->image)
					                <td><img src="{{ asset('content_img/'.$row->image) }}" alt="" width="160" height="160"></td>
					                @else
					                <td>No IMage Added</td>
					                @endif
					                <td>
					                    <button wire:click="edit({{$row->id}})" class="btn btn-sm btn-outline-primary py-0">Edit</button> | 
					                    <button wire:click="destroy({{$row->id}})" class="btn btn-sm btn-outline-primary py-0">Delete</button>
					                </td>
					            </tr>
					        @endforeach
					    </table>

					</div>

                </div>
            </div>
        </div>
    </div>
</div>
 