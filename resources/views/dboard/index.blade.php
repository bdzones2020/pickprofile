@extends('layouts.app')

@section('content')

@php $appInfo = appInfo() @endphp

    @include('dboard.inc.header')

    <main class="main-content">
        
        <!-- wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper" class="bg-dark">
                @include('dboard.inc.sidebar')
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">
                @yield('dboard_content')

                <!-- Footer -->
                <footer class="main-footer">
                    <div class="pull-right hidden-xs">Developed by <b>BDZONES</b></div>
                    <strong>{{ date('Y') }} &copy {{ !empty($appInfo->name) ? $appInfo->name : config('app.name') }}.</strong> All rights reserved.
                </footer>
                <!-- /#Footer -->
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- /#wrapper -->
        
    </main>

@endsection

@section('scripts')

<script>
    $(document).ready(function() {
        $("#sidebarTogglerBtn").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    });
</script>

@endsection