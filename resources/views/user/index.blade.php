@extends('layouts.app')

@section('styles')

	<link href="{{ asset('css/profile.css') }}" rel="stylesheet">

@endsection

@section('content')

@php $appInfo = appInfo() @endphp

    @include('user.inc.header')

    <main class="main-content">
        
        <!-- wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper" class="bg-dark">
                @livewire('user.inc.sidebar')
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">
                @yield('user_content')

                <hr>

                <!-- Footer -->
                <footer class="main-footer">
                    <div class="pull-right hidden-xs">Developed by <b>BDZONES</b></div>
                    <strong>{{ date('Y') }} &copy {{ !empty($appInfo->name) ? $appInfo->name : config('app.name') }}.</strong> All rights reserved.
                </footer>
                <!-- /#Footer -->
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- /#wrapper -->
        
    </main>

@endsection

@section('scripts')

<script>
	$(document).ready(function() {
		//
	});
</script>

@endsection