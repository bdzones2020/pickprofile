@extends('user.index')

@section('title','Profile')

@section('user_content')

		@livewire('user.profile')

@endsection

@section('scripts')
	<script>
		$(document).ready(function(){
			// tooltip
			$('[data-toggle="tooltip"]').tooltip({
				trigger : 'hover'
			});

			$("#profileSettingBtn").click(function() {
		        //e.preventDefault();
		        $('#profileSettingsModal').modal({backdrop: "static"});
		    });
		});
	</script>
@endsection