<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_settings', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned()->index()->nullable()->default(0);
            $table->integer('property_id')->unsigned()->index()->nullable()->default(0);
            $table->boolean('status')->nullable()->default(true);
            $table->tinyinteger('privacy')->nullable()->default(0);
            $table->tinyinteger('serial_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_settings');
    }
}
