<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned()->index()->nullable()->default(0);
            $table->integer('language_id')->unsigned()->index()->nullable()->default(0);
            $table->string('speaking')->nullable();
            $table->string('reading')->nullable();
            $table->string('writing')->nullable();
            $table->string('listening')->nullable();
            $table->string('remarks')->nullable();
            $table->integer('serial_no')->nullable()->default(0);
            $table->string('privacy')->nullable();
            $table->boolean('status')->nullable()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_languages');
    }
}
