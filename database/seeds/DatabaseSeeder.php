<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\User\Profile;
use App\Models\AppInfo;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $passSystem = 789456123;
        $userSystem = User::create([
            'user_name' => 'System Admin',
            'email' => 'system.admin@mailinator.com',
            'type' => 'system',
            'weight' => 99.99,
            'status' => 1,
            'password' => bcrypt($passSystem),
            //'remember_token' => str_random(10),
        ]);

        $userSystem->profile = Profile::create([
            'user_id' => $userSystem->id,
            'first_name' => 'System',
            'last_name' => 'Admin',
            //'contact_no' => $user->contact_no,
            'contact_email' => $userSystem->email,
            //'mailing_address' => $data['mailing_address'],
        ]);

        $passRecruit = '123456789';
        $userRecruiter = User::create([
            'user_name' => 'Test Recruiter',
            'email' => 'recruiter@mailinator.com',
            'type' => 'recruiter',
            'weight' => 59.99,
            'status' => 1,
            'password' => bcrypt($passRecruit),
            //'remember_token' => str_random(10),
        ]);

        $userRecruiter->profile = Profile::create([
            'user_id' => $userRecruiter->id,
            'first_name' => 'Test',
            'last_name' => 'Recruiter',
            //'contact_no' => $user->contact_no,
            'contact_email' => $userRecruiter->email,
            //'photo' => 'Ashraf_Khan2019_09_20_08_37_21.jpg',
            //'mailing_address' => $data['mailing_address'],
        ]);

        $passUser = '123456789';
        $testUser = User::create([
            'user_name' => 'Test User',
            'email' => 'user@mailinator.com',
            'type' => 'user',
            'weight' => 9.99,
            'status' => 1,
            'password' => bcrypt($passRecruit),
            //'remember_token' => str_random(10),
        ]);

        $testUser->profile = Profile::create([
            'user_id' => $testUser->id,
            'first_name' => 'Test',
            'last_name' => 'User',
            //'contact_no' => $user->contact_no,
            'contact_email' => $testUser->email,
            //'photo' => '',
            //'mailing_address' => $data['mailing_address'],
        ]);

        // app info
        // $appInfo = AppInfo::create([
        //     'name' => 'Application 6.0',
        //     'contact_no' => '0123456789',
        //     'email' => 'app.info@mailtrap.com',
        //     'base_currency' => 'BDT',
        //     'logo' => 'sample_logo.jpg',
        //     'status' => '1'
        // ]);

        $this->call('PropertySeeder');
    }
}
