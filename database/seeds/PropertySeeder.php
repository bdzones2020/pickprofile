<?php

use Illuminate\Database\Seeder;
use App\Models\User\Property;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $property = Property::create(['name' => 'DOB']);
        $property = Property::create(['name' => 'CONTACT_INFO']);
        $property = Property::create(['name' => 'SOCIAL_NETWORK']);
        $property = Property::create(['name' => 'INTEREST']);
        $property = Property::create(['name' => 'EDUCATION']);
        $property = Property::create(['name' => 'EXPERIENCE']);
        $property = Property::create(['name' => 'LANGUAGE']);
        $property = Property::create(['name' => 'CERTIFICATION']);
        $property = Property::create(['name' => 'COURSE']);
        $property = Property::create(['name' => 'PATENT']);
        $property = Property::create(['name' => 'PUBLICATION']);
        $property = Property::create(['name' => 'ORGANIZATION_CLUB']);
        $property = Property::create(['name' => 'HONOR_AWARD']);
        $property = Property::create(['name' => 'TEST']);
        $property = Property::create(['name' => 'PROJECT']);
        $property = Property::create(['name' => 'VOLANTEER_SOCIALWORK']);
        $property = Property::create(['name' => 'EXTRA_CURRICULUMN']);
        $property = Property::create(['name' => 'RECOMMANDATION']);
    }
}
