<?php

namespace App\Models\Analysis;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Stat extends Model
{
    protected $fillable = ['user_id','total_view','activity_rate','response_rate','points'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}