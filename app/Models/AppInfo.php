<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppInfo extends Model
{
    protected $fillable = ['name', 'subtitle', 'logo', 'contact_no', 'email', 'website', 'address', 'timezone', 'base_currency', 'lang', 'details', 'map', 'status'];
}