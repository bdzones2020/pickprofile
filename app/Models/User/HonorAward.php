<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class HonorAward extends Model
{
    protected $fillable = ['user_id','title','honor','award','type','issuer','issue_date','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}