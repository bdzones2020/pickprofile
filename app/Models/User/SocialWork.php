<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class SocialWork extends Model
{
    protected $fillable = ['user_id','name','type','location','activities','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
