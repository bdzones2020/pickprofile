<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Certification extends Model
{
    protected $fillable = ['user_id','title','type','institute','issue_date','remarks','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
