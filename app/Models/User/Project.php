<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Project extends Model
{
    protected $fillable = ['user_id','name','type','start_date','end_date','project_members','project_url','currently_working','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}