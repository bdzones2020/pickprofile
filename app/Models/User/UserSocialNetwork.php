<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Content\SocialNetwork;

class UserSocialNetwork extends Model
{
    protected $fillable = ['user_id','social_network_id','url','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function social_network()
    {
    	return $this->belongsTo(SocialNetwork::class);
    }
}