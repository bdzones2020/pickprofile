<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Models\Content\Religion;
use App\Models\Content\Profession;
use App\Models\User\UserSkill;
use App\Models\User\Experience;

class Profile extends Model
{
    protected $fillable = ['user_id','first_name','middle_name','last_name','sarname','forename','father_name','mother_name','gender','dob','religion_id','profession_id','photo','nid','passport_no','driving_license_no','contact_no','contact_email','country','state','zip_code','permanent_address','present_address','interests','schedule_type'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function religion()
    {
    	return $this->belongsTo(Religion::class);
    }

    public function profession()
    {
    	return $this->belongsTo(Profession::class);
    }

    public function user_skills()
    {
        return $this->hasMany(UserSkill::class, 'user_id', 'user_id')->select('id','user_id','skill_id','details','privacy','status')->orderBy('serial_no','ASC');
    }

    public function experiences()
    {
        return $this->hasMany(Experience::class, 'user_id', 'user_id')->select('id','user_id','company','location','designation','joined','left','details','serial_no','privacy','status')->orderBy('serial_no','ASC');
    }

    public function profile_settings()
    {
        return $this->hasMany(ProfileSettingsModel::class, 'user_id', 'user_id');
    }
}