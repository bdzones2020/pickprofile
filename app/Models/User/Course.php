<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Course extends Model
{
	protected $fillable = ['user_id','name','type','institute','duration','start_date','end_date','remarks','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}