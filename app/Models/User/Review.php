<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Review extends Model
{
    protected $fillable = ['user_id','reviewer_name','reviewer_email','reviewer_contact_no','reviewer_location','reviewer_address','review','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}