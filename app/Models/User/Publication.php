<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Publication extends Model
{
    protected $fillable = ['user_id','title','publisher','publication_date','publication_url','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}