<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Content\Language;

class UserLanguage extends Model
{
    protected $fillable = ['user_id','language_id','speaking','reading','writing','listening','remarks','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function language()
    {
    	return $this->belongsTo(Language::class);
    }
}