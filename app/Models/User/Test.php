<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Test extends Model
{
    protected $fillable = ['user_id','name','type','test_date','score','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}