<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Idea extends Model
{
    protected $fillable = ['user_id','title','type','purpose','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}