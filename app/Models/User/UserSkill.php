<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Models\Content\Skill;

class UserSkill extends Model
{
    protected $fillable = ['user_id','skill_id','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function skill()
    {
    	return $this->belongsTo(Skill::class)->select('id','name');
    }
}