<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Patent extends Model
{
    protected $fillable = ['user_id','title','location','patent_no','patent_members','patent_status','issue_date','patent_url','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}