<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Models\User\Profile;
use App\Models\User\Property;

class ProfileSettingsModel extends Model
{
	protected $table = 'profile_settings';

    protected $fillable = ['user_id','property_id', 'status','privacy','serial_no'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function property()
    {
    	return $this->belongsTo(Property::class);
    }

    public function profile()
    {
    	return $this->hasOne(Profile::class, 'user_id', 'user_id');
    }
}