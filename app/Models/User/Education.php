<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Education extends Model
{
    protected $fillable = ['user_id','school','degree','field_of_study','started','ended','grade','grade_point','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}