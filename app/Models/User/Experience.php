<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Experience extends Model
{
    protected $fillable = ['user_id','company','type','location','designation','joined','left','details','serial_no','privacy','status'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}