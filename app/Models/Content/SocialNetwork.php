<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;

use App\User;

class SocialNetwork extends Model
{
    protected $fillable = ['name','type','serial_no','status'];

    public function user()
    {
    	return $this->hasMany(User::class);
    }
}