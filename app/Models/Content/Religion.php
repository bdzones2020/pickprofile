<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Religion extends Model
{
    protected $fillable = ['name','serial_no','status'];

    public function user()
    {
    	return $this->hasMany(User::class);
    }
}