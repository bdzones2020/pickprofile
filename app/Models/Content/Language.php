<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Language extends Model
{
    protected $fillable = ['name','type','weight','serial_no','status'];

    public function user()
    {
    	return $this->hasMany(User::class);
    }
}