<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;

use App\Models\User\Profile;

class Profession extends Model
{
    protected $fillable = ['name','serial_no','status'];

    public function profile()
    {
    	return $this->hasMany(Profile::class);
    }
}
