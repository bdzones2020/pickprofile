<?php

namespace App;

use App\Models\User\Profile;
use App\Models\User\Experience;
use App\Models\User\Education;
use App\Models\User\UserSkill;
use App\Models\User\UserLanguage;
use App\Models\User\Publication;
use App\Models\User\Course;
use App\Models\User\Project;
use App\Models\User\Patent;
use App\Models\User\Organization;
use App\Models\User\HonorAward;
use App\Models\Analysis\Stat;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name', 'email', 'password', 'remember_token', 'contact_no', 'options', 'type', 'weight', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * related objects for database relation
     *
     * @var object
     */

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function experiences()
    {
        return $this->hasMany(Experience::class);
    }

    public function educations()
    {
        return $this->hasMany(Education::class);
    }

    public function user_skills()
    {
        return $this->hasMany(UserSkill::class);
    }

    public function user_languages()
    {
        return $this->hasMany(UserLanguage::class);
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function publications()
    {
        return $this->hasMany(Publication::class);
    }

    public function patents()
    {
        return $this->hasMany(Patent::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }

    public function honor_awards()
    {
        return $this->hasMany(HonorAward::class);
    }

    public function stat()
    {
        return $this->hasOne(Stat::class);
    }
}
