<?php

namespace App\Http\Livewire\Post;

use App\Models\MessageModel;
use Livewire\Component;

use Image;
use Storage;

class Message extends Component
{
	public $data, $name, $email, $contact_no, $message, $image, $oldImage, $selected_id, $search;
	public $createMode = false;
	public $updateMode = false;
	public $showMode = true;
	public $closeMode = false;
    public $fileSize;

    protected $listeners = ['imageUpload' => 'processChosenImage'];

    public function processChosenImage($chosenImage)
    {
        $this->image = $chosenImage;
    }

    public function render()
    {
    	if($this->search)
        {
            $this->data = MessageModel::where('name','LIKE','%'.$this->search.'%')->orderBy('id','DESC')->get();
        }else{
            $this->data = MessageModel::orderBy('id','DESC')->get();
        }
        return view('livewire.post.message');
    }

    private function resetInput()
    {
    	$this->name = null;
    	$this->email = null;
    	$this->contact_no = null;
    	$this->message = null;
        $this->image = null;
        $this->search = null;
    }

    public function close()
    {
    	$this->closeMode = false;
    	$this->createMode = false;
    	$this->showMode = true;
    	$this->updateMode = false;
    }

    public function uploadImage()
    {
        if(!$this->image)
        {
            return null;
        }

        $storeImage = Image::make($this->image)->encode('png');

        //$imageName = date('ymdhis').'.'.explode('/', $storeImage->mime())[1];
        $imageName = date('ymdhis').'.png';

        Storage::disk('content_img')->put($imageName, $storeImage);

        return $imageName;

    }

    public function create()
    {
    	$this->createMode = true;
    	$this->showMode = false;
    	$this->closeMode = true;

        $this->resetInput();
    }

    public function store()
    {
    	$this->validate([
    		'name' => 'required|min:5',
    		'email' => 'required|email:rfc,dns',
    		'contact_no' => 'required|min:11',
    		'message' => 'required|min:2',
    	]);

        $image = $this->uploadImage();

    	MessageModel::create([
    		'name' => $this->name,
    		'email' => $this->email,
    		'contact_no' => $this->contact_no,
    		'message' => $this->message,
            'image' => $image,
    	]);
    	$this->updateMode = false;
		$this->createMode = false;
		$this->showMode = true;
		$this->closeMode = false;
    	$this->resetInput();
    }

    public function edit($id)
    {
    	$record = MessageModel::findOrFail($id);
    	$this->selected_id = $record->id;
    	$this->name = $record->name;
    	$this->email = $record->email;
    	$this->contact_no = $record->contact_no;
    	$this->message = $record->message;
        $this->oldImage = $record->image;
    	$this->updateMode = true;
    	$this->createMode = false;
    	$this->showMode = false;
    	$this->closeMode = true;
    }

    public function update()
    {
    	$this->validate([
    		'selected_id' => 'required',
    		'name' => 'required|min:5',
            'email' => 'required|email:rfc,dns',
            'contact_no' => 'required|min:11',
            'message' => 'required|min:2',
    	]);

    	if($this->selected_id)
    	{
    		$record = MessageModel::find($this->selected_id);

            if($this->image)
            {
                if (Storage::disk('content_img')->has($record->image)) {
                    Storage::disk('content_img')->delete($record->image);
                }
            }

            $image = $this->uploadImage();

    		$record->update([
    			'name' => $this->name,
    			'email' => $this->email,
    			'contact_no' => $this->contact_no,
    			'message' => $this->message,
                'image' => $image,
    		]);
    		$this->resetInput();
    		$this->updateMode = false;
    		$this->createMode = false;
    		$this->showMode = true;
    		$this->closeMode = false;
    	}
    }

    public function destroy($id)
    {
    	if($id)
    	{
    		$record = MessageModel::find($id);

            if (Storage::disk('content_img')->has($record->image)) {
                Storage::disk('content_img')->delete($record->image);
            }

    		$record->delete();
    	}
    }
}
