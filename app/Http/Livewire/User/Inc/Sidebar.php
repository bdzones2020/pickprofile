<?php

namespace App\Http\Livewire\User\Inc;

use Livewire\Component;

class Sidebar extends Component
{
	public $data;

    public function render()
    {
        return view('livewire.user.inc.sidebar');
    }
}
