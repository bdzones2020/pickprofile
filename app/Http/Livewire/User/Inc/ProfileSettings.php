<?php

namespace App\Http\Livewire\User\Inc;

use Livewire\Component;
use App\Models\User\ProfileSettingsModel;
use App\Models\User\Property;
use App\Models\User\Profile;
use Auth;

class ProfileSettings extends Component
{
	public $profileSettings;
	public $dobId, $dobStatus, $dobPrivacy;
	public $contactInfoId, $contactInfoStatus, $contactInfoPrivacy;
	public $interestId, $interestStatus, $interestPrivacy;
	public $socialNetworkId, $socialNetworkStatus, $socialNetworkPrivacy;
	public $educationId, $educationStatus, $educationPrivacy, $educationSerialNo;
	public $experienceId, $experienceStatus, $experiencePrivacy, $experienceSerialNo;
	public $languageId, $languageStatus, $languagePrivacy, $languageSerialNo;
	public $certificationId, $certificationStatus, $certificationPrivacy, $certificationSerialNo;
	public $courseId, $courseStatus, $coursePrivacy, $courseSerialNo;
	public $patentId, $patentStatus, $patentPrivacy, $patentSerialNo;
	public $publicationId, $publicationStatus, $publicationPrivacy, $publicationSerialNo;
	public $organizationClubId, $organizationClubStatus, $organizationClubPrivacy, $organizationClubSerialNo;
	public $honorAwardId, $honorAwardStatus, $honorAwardPrivacy, $honorAwardSerialNo;
	public $testId, $testStatus, $testPrivacy, $testSerialNo;
	public $projectId, $projectStatus, $projectPrivacy, $projectSerialNo;
	public $volanteerSocialworkId, $volanteerSocialworkStatus, $volanteerSocialworkPrivacy, $volanteerSocialworkSerialNo;
	public $extraCurriculumnId, $extraCurriculumnStatus, $extraCurriculumnPrivacy, $extraCurriculumnSerialNo;
	public $recommandationId, $recommandationStatus, $recommandationPrivacy, $recommandationSerialNo;
	public $editMode = false;
	public $properties = [];

	protected $listeners = ['editProfileSettings' => 'editProfileSettings'];

    public function render()
    {
        return view('livewire.user.inc.profile-settings');
    }

    public function getPropertyIds($id)
    {
    	if(!in_array($id, $this->properties))
    	{
    		array_push($this->properties, $id);
    	}
    }

    public function editProfileSettings()
    {
    	$records = ProfileSettingsModel::with('property')->where('user_id',Auth::user()->id)->get();

        foreach ($records as $propertySetting) {

            if($propertySetting->property->name == 'DOB'){
                $this->dobId = $propertySetting->id;
                $this->dobStatus = $propertySetting->status;
                $this->dobPrivacy = $propertySetting->privacy;
            }elseif($propertySetting->property->name == 'CONTACT_INFO'){
                $this->contactInfoId = $propertySetting->id;
                $this->contactInfoStatus = $propertySetting->status;
                $this->contactInfoPrivacy = $propertySetting->privacy;
            }elseif($propertySetting->property->name == 'SOCIAL_NETWORK'){
                $this->socialNetworkId = $propertySetting->id;
                $this->socialNetworkStatus = $propertySetting->status;
                $this->socialNetworkPrivacy = $propertySetting->privacy;
            }elseif($propertySetting->property->name == 'INTEREST'){
                $this->interestId = $propertySetting->id;
                $this->interestStatus = $propertySetting->status;
                $this->interestPrivacy = $propertySetting->privacy;
            }elseif($propertySetting->property->name == 'EDUCATION'){
                $this->educationId = $propertySetting->id;
                $this->educationStatus = $propertySetting->status;
                $this->educationPrivacy = $propertySetting->privacy;
                $this->educationSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'EXPERIENCE'){
                $this->experienceId = $propertySetting->id;
                $this->experienceStatus = $propertySetting->status;
                $this->experiencePrivacy = $propertySetting->privacy;
                $this->experienceSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'LANGUAGE'){
                $this->languageId = $propertySetting->id;
                $this->languageStatus = $propertySetting->status;
                $this->languagePrivacy = $propertySetting->privacy;
                $this->languageSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'CERTIFICATION'){
                $this->certificationId = $propertySetting->id;
                $this->certificationStatus = $propertySetting->status;
                $this->certificationPrivacy = $propertySetting->privacy;
                $this->certificationSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'COURSE'){
                $this->courseId = $propertySetting->id;
                $this->courseStatus = $propertySetting->status;
                $this->coursePrivacy = $propertySetting->privacy;
                $this->courseSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'PATENT'){
                $this->patentId = $propertySetting->id;
                $this->patentStatus = $propertySetting->status;
                $this->patentPrivacy = $propertySetting->privacy;
                $this->patentSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'PUBLICATION'){
                $this->publicationId = $propertySetting->id;
                $this->publicationStatus = $propertySetting->status;
                $this->publicationPrivacy = $propertySetting->privacy;
                $this->publicationSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'ORGANIZATION_CLUB'){
                $this->organizationClubId = $propertySetting->id;
                $this->organizationClubStatus = $propertySetting->status;
                $this->organizationClubPrivacy = $propertySetting->privacy;
                $this->organizationClubSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'HONOR_AWARD'){
                $this->honorAwardId = $propertySetting->id;
                $this->honorAwardStatus = $propertySetting->status;
                $this->honorAwardPrivacy = $propertySetting->privacy;
                $this->honorAwardSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'TEST'){
                $this->testId = $propertySetting->id;
                $this->testStatus = $propertySetting->status;
                $this->testPrivacy = $propertySetting->privacy;
                $this->testSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'PROJECT'){
                $this->projectId = $propertySetting->id;
                $this->projectStatus = $propertySetting->status;
                $this->projectPrivacy = $propertySetting->privacy;
                $this->projectSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'VOLANTEER_SOCIALWORK'){
                $this->volanteerSocialworkId = $propertySetting->id;
                $this->volanteerSocialworkStatus = $propertySetting->status;
                $this->volanteerSocialworkPrivacy = $propertySetting->privacy;
                $this->volanteerSocialworkSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'EXTRA_CURRICULUMN'){
                $this->extraCurriculumnId = $propertySetting->id;
                $this->extraCurriculumnStatus = $propertySetting->status;
                $this->extraCurriculumnPrivacy = $propertySetting->privacy;
                $this->extraCurriculumnSerialNo = $propertySetting->serial_no;
            }elseif($propertySetting->property->name == 'RECOMMANDATION'){
                $this->recommandationId = $propertySetting->id;
                $this->recommandationStatus = $propertySetting->status;
                $this->recommandationPrivacy = $propertySetting->privacy;
                $this->recommandationSerialNo = $propertySetting->serial_no;
            }
        }

		$this->editMode = true;
    }

    public function update()
    {
    	if($this->properties && count($this->properties) > 0)
    	{
    		foreach ($this->properties as $k => $id) {
	    		$profileProperty = ProfileSettingsModel::with('property')->find($id);
	    		
	    		if($profileProperty->property->name == 'DOB'){
	    			$profileProperty->status = $this->dobStatus;
	    			$profileProperty->privacy = $this->dobPrivacy;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'CONTACT_INFO'){
	    			$profileProperty->status = $this->contactInfoStatus;
	    			$profileProperty->privacy = $this->contactInfoPrivacy;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'SOCIAL_NETWORK'){
	    			$profileProperty->status = $this->socialNetworkStatus;
	    			$profileProperty->privacy = $this->socialNetworkPrivacy;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'INTEREST'){
	    			$profileProperty->status = $this->interestStatus;
	    			$profileProperty->privacy = $this->interestPrivacy;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'EDUCATION'){
	    			$profileProperty->status = $this->educationStatus;
	    			$profileProperty->privacy = $this->educationPrivacy;
	    			$profileProperty->serial_no = $this->educationSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'EXPERIENCE'){
	    			$profileProperty->status = $this->experienceStatus;
	    			$profileProperty->privacy = $this->experiencePrivacy;
	    			$profileProperty->serial_no = $this->experienceSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'LANGUAGE'){
	    			$profileProperty->status = $this->languageStatus;
	    			$profileProperty->privacy = $this->languagePrivacy;
	    			$profileProperty->serial_no = $this->languageSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'CERTIFICATION'){
	    			$profileProperty->status = $this->certificationStatus;
	    			$profileProperty->privacy = $this->certificationPrivacy;
	    			$profileProperty->serial_no = $this->certificationSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'COURSE'){
	    			$profileProperty->status = $this->courseStatus;
	    			$profileProperty->privacy = $this->coursePrivacy;
	    			$profileProperty->serial_no = $this->courseSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'PATENT'){
	    			$profileProperty->status = $this->patentStatus;
	    			$profileProperty->privacy = $this->patentPrivacy;
	    			$profileProperty->serial_no = $this->patentSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'PUBLICATION'){
	    			$profileProperty->status = $this->publicationStatus;
	    			$profileProperty->privacy = $this->publicationPrivacy;
	    			$profileProperty->serial_no = $this->publicationSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'ORGANIZATION_CLUB'){
	    			$profileProperty->status = $this->organizationClubStatus;
	    			$profileProperty->privacy = $this->organizationClubPrivacy;
	    			$profileProperty->serial_no = $this->organizationClubSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'HONOR_AWARD'){
	    			$profileProperty->status = $this->honorAwardStatus;
	    			$profileProperty->privacy = $this->honorAwardPrivacy;
	    			$profileProperty->serial_no = $this->honorAwardSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'TEST'){
	    			$profileProperty->status = $this->testStatus;
	    			$profileProperty->privacy = $this->testPrivacy;
	    			$profileProperty->serial_no = $this->testSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'PROJECT'){
	    			$profileProperty->status = $this->projectStatus;
	    			$profileProperty->privacy = $this->projectPrivacy;
	    			$profileProperty->serial_no = $this->projectSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'VOLANTEER_SOCIALWORK'){
	    			$profileProperty->status = $this->volanteerSocialworkStatus;
	    			$profileProperty->privacy = $this->volanteerSocialworkPrivacy;
	    			$profileProperty->serial_no = $this->volanteerSocialworkSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'EXTRA_CURRICULUMN'){
	    			$profileProperty->status = $this->extraCurriculumnStatus;
	    			$profileProperty->privacy = $this->extraCurriculumnPrivacy;
	    			$profileProperty->serial_no = $this->extraCurriculumnSerialNo;
	    			$profileProperty->update();
	    		}elseif($profileProperty->property->name == 'RECOMMANDATION'){
	    			$profileProperty->status = $this->recommandationStatus;
	    			$profileProperty->privacy = $this->recommandationPrivacy;
	    			$profileProperty->serial_no = $this->recommandationSerialNo;
	    			$profileProperty->update();
	    		}
	    	}
    	}

    	$editMode = false;

		$this->emit('refreshProfile');
    }
}