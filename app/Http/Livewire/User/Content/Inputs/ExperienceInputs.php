<?php

namespace App\Http\Livewire\User\Content\Inputs;

use Livewire\Component;
use Auth;
use App\Models\User\Experience;

class ExperienceInputs extends Component
{
	public $inputMode, $company, $location, $designation, $joined, $left, $stillWorking, $details, $serialNo, $privacy, $status, $selectedId;

	protected $listeners = ['createExperience' => 'create', 'editExperience' => 'edit'];

    public function render()
    {
        return view('livewire.user.content.inputs.experience-inputs');
    }

    public function create()
    {
    	$this->company = $this->location = $this->designation = $this->joined = $this->left = $this->details = '';
    	$this->stillWorking = false;
    	$this->serialNo = 0;
    	$this->privacy = 0;
    	$this->status = true;
    	$this->inputMode = 'Add';
    }

    public function edit($id)
    {
    	$experience = Experience::find($id);
    	$this->company = $experience->company;
    	$this->location = $experience->location;
    	$this->designation = $experience->designation;
    	$this->joined = date('d-m-Y', strtotime($experience->joined));
    	$this->left = $experience->left ? date('d-m-Y', strtotime($experience->left)) : null ;
    	$this->details = $experience->details;
    	$this->stillWorking = $experience->left ? false : true ;
    	$this->serialNo = $experience->serial_no;
    	$this->privacy = $experience->privacy;
    	$this->status = $experience->status;
    	$this->selectedId = $experience->id;
    	$this->inputMode = 'Edit';
    }

    public function cancel()
    {
    	$this->inputMode = false;
    	$this->emit('cancelExperience');
    }

    public function save()
    {
    	$experience = !$this->selectedId ? new Experience : Experience::find($this->selectedId);
    	$experience->user_id = Auth::user()->id;
    	$experience->company = $this->company;
    	$experience->location = $this->location;
    	$experience->designation = $this->designation;
    	$experience->joined = date('Y-m-d', strtotime($this->joined));
    	$experience->left = $this->left ? date('Y-m-d', strtotime($this->left)) : NULL ;
    	$experience->details = $this->details ? $this->details : NULL ;
    	$experience->serial_no = $this->serialNo ? $this->serialNo : NULL ;
    	$experience->privacy = $this->privacy;
    	$experience->status = $this->status;
    	$experience->save();
        $this->inputMode = false;
    	
    	$this->emit('refreshProfile');
    }
}