<?php

namespace App\Http\Livewire\User\Content\Inputs;

use Livewire\Component;
use Auth;
use App\Models\User\Education;

class EducationInputs extends Component
{
    public $inputMode, $years, $school, $degree, $fieldOfStudy, $started, $ended, $stillStudying, $grade, $gradePoint, $details, $serialNo, $privacy, $status, $selectedId;

	protected $listeners = ['createEducation' => 'create', 'editEducation' => 'edit'];

    public function render()
    {
        return view('livewire.user.content.inputs.education-inputs');
    }

    public function create()
    {
    	$this->school = $this->degree = $this->fieldOfStudy = $this->started = $this->ended = $this->details = $this->grade = $this->gradePoint = '';
    	$this->stillStudying = false;
    	$this->serialNo = 0;
    	$this->privacy = 0;
    	$this->status = true;
    	$this->inputMode = 'Add';
    	$this->years = $this->getYears();
    }

    public function edit($id)
    {
    	$education = Education::find($id);
    	$this->school = $education->school;
    	$this->degree = $education->degree;
    	$this->fieldOfStudy = $education->field_of_study;
    	$this->started = $education->started;
    	$this->stillStudying = $education->ended ? false : true ;
    	$this->ended = $education->ended;
    	$this->grade = $education->grade;
    	$this->gradePoint = $education->grade_point;
    	$this->details = $education->details;
    	$this->serialNo = $education->serial_no;
    	$this->privacy = $education->privacy;
    	$this->status = $education->status;
    	$this->selectedId = $education->id;
    	$this->inputMode = 'Edit';
    	$this->years = $this->getYears();
    }

    public function cancel()
    {
    	$this->inputMode = false;
    	$this->emit('cancelEducation');
    }

    public function save()
    {
    	$education = !$this->selectedId ? new education : education::find($this->selectedId);
    	$education->user_id = Auth::user()->id;
    	$education->school = strtoupper($this->school);
    	$education->degree = strtoupper($this->degree);
    	$education->field_of_study = strtoupper($this->fieldOfStudy);
    	$education->started = $this->started;
    	$education->ended = $this->ended ? $this->ended : NULL ;
    	$education->grade = $this->grade ? strtoupper($this->grade) : NULL ;
    	$education->grade_point = $this->gradePoint ? $this->gradePoint : NULL ;
    	$education->details = $this->details ? $this->details : NULL ;
    	$education->serial_no = $this->serialNo ? $this->serialNo : NULL ;
    	$education->privacy = $this->privacy;
    	$education->status = $this->status;
    	$education->save();
    	$this->inputMode = false;
    	
    	$this->emit('refreshProfile');
    }

    private function getYears()
    {
    	$yearArray = [];
    	for ($startYear=1970; $startYear < date('Y'); $startYear++) { 
    		$yearArray[$startYear] = $startYear;
    	}
    	return $yearArray;
    }
}
