<?php

namespace App\Http\Livewire\User\Content\Inputs;

use Livewire\Component;
use Auth;
use App\Models\User\Profile;

class ContactInfoInputs extends Component
{
    public $contactNo, $contactEmail, $presentAddress;

    protected $listeners = ['editContactInfo' => 'edit'];

    public function render()
    {
        return view('livewire.user.content.inputs.contact-info-inputs');
    }

    public function edit()
    {
        $profile = Auth::User()->profile;
        $this->contactNo = $profile->contact_no;
        $this->contactEmail = $profile->contact_email;
        $this->presentAddress = $profile->present_address;
    }

    public function cancel()
    {
        $this->emit('cancelContactInfo');
    }

    public function update()
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        $profile->contact_no = $this->contactNo;
        $profile->contact_email = $this->contactEmail;
        $profile->present_address = $this->presentAddress;
        $profile->update();

        $this->emit('refreshProfile');
    }
}
