<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

use Auth;
use App\Models\User\ProfileSettingsModel;
use App\Models\User\Property;
use App\Models\User\Experience;
use App\Models\User\Education;

class Profile extends Component
{
    public $profile;
    public $profileSettings, $settingHelpers, $experiences, $educations;
    public $profileDobId, $profileDobPrivacy, $profileDobStatus;
    public $profileContactInfoId, $profileContactInfoPrivacy, $profileContactInfoStatus;
    public $profileSocialNetworkId, $profileSocialNetworkPrivacy, $profileSocialNetworkStatus;
    public $profileInterestId, $profileInterestPrivacy, $profileInterestStatus;
    public $experienceInput = false;
    public $educationInput = false;
    public $contactInfoInput = false;


    protected $listeners = ['updateProfile' => 'updateProfile', 'refreshProfile' => 'refresh', 'cancelExperience', 'cancelEducation', 'cancelContactInfo'];

    public function render()
    {
        $this->profile =  Auth::User()->profile;
        $this->profileSettings = ProfileSettingsModel::with('property')->where('user_id',Auth::user()->id)->where('property_id', '>', '4')->orderBy('serial_no','ASC')->get();
        $this->settingHelpers = ProfileSettingsModel::with('property')->where('user_id',Auth::user()->id)->where('property_id', '<', '5')->orderBy('serial_no','ASC')->get();

        foreach ($this->settingHelpers as $s => $settingHelper) {
            if($settingHelper->property->name == 'DOB')
            {
                $this->profileDobId = $settingHelper->id;
                $this->profileDobPrivacy = $settingHelper->privacy;
                $this->profileDobStatus = $settingHelper->status;
            }elseif($settingHelper->property->name == 'CONTACT_INFO')
            {
                $this->profileContactInfoId = $settingHelper->id;
                $this->profileContactInfoPrivacy = $settingHelper->privacy;
                $this->profileContactInfoStatus = $settingHelper->status;
            }elseif($settingHelper->property->name == 'SOCIAL_NETWORK')
            {
                $this->profileSocialNetworkId = $settingHelper->id;
                $this->profileSocialNetworkPrivacy = $settingHelper->privacy;
                $this->profileSocialNetworkStatus = $settingHelper->status;
            }elseif($settingHelper->property->name == 'INTEREST')
            {
                $this->profileInterestId = $settingHelper->id;
                $this->profileInterestPrivacy = $settingHelper->privacy;
                $this->profileInterestStatus = $settingHelper->status;
            }
        }

        $this->experiences = Experience::where('user_id', Auth::user()->id)->orderBy('serial_no','ASC')->get();
        $this->educations = Education::where('user_id', Auth::user()->id)->orderBy('serial_no','ASC')->get();

        return view('livewire.user.profile');
    }

    public function refresh()
    {
        $this->profile =  Auth::User()->profile;
        $this->profileSettings = ProfileSettingsModel::with('property')->where('user_id',Auth::user()->id)->where('property_id', '>', '4')->orderBy('serial_no','ASC')->get();
        $this->settingHelpers = ProfileSettingsModel::with('property')->where('user_id',Auth::user()->id)->where('property_id', '<', '5')->orderBy('serial_no','ASC')->get();

        foreach ($this->settingHelpers as $s => $settingHelper) {
            if($settingHelper->property->name == 'DOB')
            {
                $this->profileDobId = $settingHelper->id;
                $this->profileDobPrivacy = $settingHelper->privacy;
                $this->profileDobStatus = $settingHelper->status;
            }elseif($settingHelper->property->name == 'CONTACT_INFO')
            {
                $this->profileContactInfoId = $settingHelper->id;
                $this->profileContactInfoPrivacy = $settingHelper->privacy;
                $this->profileContactInfoStatus = $settingHelper->status;
            }elseif($settingHelper->property->name == 'SOCIAL_NETWORK')
            {
                $this->profileSocialNetworkId = $settingHelper->id;
                $this->profileSocialNetworkPrivacy = $settingHelper->privacy;
                $this->profileSocialNetworkStatus = $settingHelper->status;
            }elseif($settingHelper->property->name == 'INTEREST')
            {
                $this->profileInterestId = $settingHelper->id;
                $this->profileInterestPrivacy = $settingHelper->privacy;
                $this->profileInterestStatus = $settingHelper->status;
            }
        }
        
        $this->experienceInput = false;
        $this->educationInput = false;
        $this->contactInfoInput = false;
    }

    // Profile Settings
    public function editProfileSettings()
    {
        $this->emit('editProfileSettings');
    }

    // Contact Info
    public function editContactInfo()
    {
        $this->contactInfoInput = true;
        $this->emit('editContactInfo');
    }

    public function cancelContactInfo()
    {
        $this->contactInfoInput = false;
    }

    // Experience
    public function createExperience()
    {
        $this->experienceInput = true;
        $this->emit('createExperience');
    }

    public function editExperience($experienceId)
    {
        $this->experienceInput = true;
        $this->emit('editExperience', $experienceId);
    }

    public function cancelExperience()
    {
        $this->experienceInput = false;
    }

    public function deleteExperience($experienceId)
    {
        $experience = Experience::find($experienceId);
        $experience->delete();
        $this->refresh();
    }

    // Education
    public function createEducation()
    {
        $this->educationInput = true;
        $this->emit('createEducation');
    }

    public function editEducation($educationId)
    {
        $this->educationInput = true;
        $this->emit('editEducation', $educationId);
    }

    public function cancelEducation()
    {
        $this->educationInput = false;
    }

    public function deleteEducation($educationId)
    {
        $education = Education::find($educationId);
        $education->delete();
        $this->refresh();
    }
}
