<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->weight >= 79.99 && Auth::user()->status == 1)
        {
            return $next($request);
        }
        return redirect('home')->with('message_warning','Sorry, you are not permitted to enter here. Please contact to your system admin.');
    }
}
