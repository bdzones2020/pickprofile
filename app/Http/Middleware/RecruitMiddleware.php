<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RecruitMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->weight >= 19.99 && Auth::user()->status == 1)
        {
            return $next($request);
        }
        return redirect('home')->with('message_warning','Sorry, you are not permitted to enter here. Please contact to your system admin.');
    }
}
