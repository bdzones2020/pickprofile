<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class SystemMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->type == 'system' && Auth::user()->weight >= 99.99)
        {
            return $next($request);
        }
        return redirect('home')->with('message_warning','Sorry, you are not permitted to enter here. Please contact to your system admin.');
    }
}
