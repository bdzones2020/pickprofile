<?php

namespace App\Http\Controllers\Dboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;

use App\Models\AppInfo;

class DboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function dboard()
    {
    	return view('dboard.dboard');
    }
}
