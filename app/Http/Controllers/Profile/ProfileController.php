<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Models\User\Profile;
use App\Models\User\ProfileSettingsModel;
use App\Models\User\Property;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('user');
    }

    public function profile()
    {
        $profile = Profile::with(['user','profession','user_skills.skill','experiences'])->where('user_id',Auth::user()->id)->first();
        $profileSettings = ProfileSettingsModel::where('user_id', $profile->user_id)->get();

        if(count($profileSettings) <= 0)
        {
            $properties = Property::get();
            $defaultSerial = 1;

            foreach ($properties as $property) {

                $profileProperty = new ProfileSettingsModel;
                $profileProperty->user_id = $profile->user_id;
                $profileProperty->property_id = $property->id;
                $profileProperty->status = $property->id <= 6 ? true : false ;
                $profileProperty->privacy = 0;
                $profileProperty->serial_no = $property->id > 4 ? $defaultSerial++ : NULL ;
                $profileProperty->save();
            }
        }
        //$countries = countries();
        return view('user.profile', compact('profile'));
    }
}
